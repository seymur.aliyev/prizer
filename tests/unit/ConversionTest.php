<?php


use app\models\Prize;

class ConversionTest extends \Codeception\Test\Unit
{

    /**
     * @dataProvider additionProvider
     */
    public function testConvert($amount, $expected)
    {
        $prize_model = new Prize();
        $bonus = $prize_model->convertMoneyToPoints($amount);
        $this->assertEquals($expected, $bonus);
    }

    public function additionProvider()
    {
        return
        [
            [1, 100],
            [2, 200],
            [3, 300],
            [4, 400]
        ];
    }
}