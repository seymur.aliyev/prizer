### Description
This is test application where registered user will win a prize of one category [ money, bonus and items]
each time when click the button `Начать розыгрыш` 

New user after registration on local project by API is registered in Dwolla sandbox payment service as customer
and gets bank account (balance). Wnen user wins money prize the amount is transfered to this balance by API, 
in case of prize is item this is sent also to user real email account as notification
, and finally bonus points are saved in local db


### Configurations
Run `composer install` command from console for upload vendor files



### Database

You can configure database by import db dump in root folder or
by running migrations


### Running  acceptance tests

Only test application has is ConversionTest.php. It's located in tests/unit/ folder
You can run in from console by  `./vendor/bin/codecept run unit ConversionTest` command


### Send email notification
You have to write down correct creditials for email sending works correctly.
In app/config/web.php inside `mailer` element modify test creditials for real ones.


### Payment service test environment
You can check customers and their transactions (transfers to their funds)
https://accounts-sandbox.dwolla.com/login
login: seymour.aliyev@yandex.ru
password: Prizer123
