<?php

/* @var $this yii\web\View */

$this->title = 'Розыгрыш призов';
?>
<div class="site-index">
    <?php if (Yii::$app->session->hasFlash('prize_item_success')): ?>
        <div class="alert alert-success alert-dismissable err_font_style alert-box font_size">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('prize_item_success');?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('prize_bonus_success')): ?>
        <div class="alert alert-success alert-dismissable err_font_style alert-box font_size">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('prize_bonus_success');?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('prize_money_success')): ?>
        <div class="alert alert-success alert-dismissable err_font_style alert-box font_size">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('prize_money_success');?>
        </div>
    <?php endif; ?>

    <div class="jumbotron mt-5">
        <h1>Добро пожаловать <?= Yii::$app->user->identity->name?></h1>
        <form method="POST" action="">
            <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                   value="<?=Yii::$app->request->csrfToken?>"/>
            <input type="submit" class="btn btn-lg btn-success" name="get_prize" value="Начать розыгрыш"/>
        </form>
    </div>
</div>
