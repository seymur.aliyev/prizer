<?php

use yii\db\Migration;

/**
 * Handles the creation of table `access_token`.
 */
class m181215_200441_create_access_token_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('access_token', [
            'id' => $this->primaryKey(),
            'token' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('access_token');
    }
}
