<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/12/2018
 * Time: 15:43
 */

namespace app\models;


use function foo\func;
use Yii;
use yii\base\Model;

class SignupForm extends Model
{
    public $name;
    public $email;
    public $password;

    public function rules(){
        return [

            [['name','email','password'], 'required'],
            [['name'], 'string'],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => 'app\models\User', 'targetAttribute' => 'email'],
        ];
    }


    public function saveNewCustomerInDB($data){
        $token_model = new AccessToken();
        $token_obj = AccessToken::findOne(1);
        \DwollaSwagger\Configuration::$access_token = $token_obj->token;// is the access_token i save in DB
        $apiClient = new \DwollaSwagger\ApiClient($token_model->sandbox_host);
        $customersApi = new \DwollaSwagger\CustomersApi($apiClient);
        $customerUrl = $token_model->createCustomer($data, $customersApi);
        if($customerUrl){
            //bank account id of created customer
            $customer_fund_id = $token_model->getCustomerFundId($apiClient, $customerUrl);

            //account info of created customer
            $new_customer = $customersApi->getCustomer($customerUrl);
            if($new_customer){
                $user = User::findOne($data['id']);
                $user->customer_id = $new_customer->id;
                $user->fund_id = $customer_fund_id;
                return $user->update(false);
            }
        }
        return false;
    }

    public function setPassword($password)
    {
        return Yii::$app->security->generatePasswordHash($password);
    }


    public function signup(){
        if($this->validate()){
            $user = new User();
            $user->name = $this->name;
            $user->email= $this->email;
            $user->password = $this->setPassword($this->password);
            if($user->create()){
                $data = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email
                ];
                $token_model = new AccessToken();
                try {
                    return $this->saveNewCustomerInDB($data);
                }
                catch (\Exception $e){
                    if($e->getCode() == 401){
                        if($token_model->createNewAccessToken()){
                            return $this->saveNewCustomerInDB($data);
                        }

                    }
                }
            }
        }
        return false;
    }
}