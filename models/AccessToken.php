<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 15.12.2018
 * Time: 4:23
 */

namespace app\models;
use Yii;

/**
 * This is the model class for table "AccessToken".
 *
 * @property integer $id
 * @property string $token
 **/

class AccessToken extends \yii\db\ActiveRecord
{
    public static function tableName(){
        return 'access_token';
    }

    public $key = 'KdapySP7pffWM9V7KxIoae56VnAWQsFEXHbyu9Ded76z6nz2Jc';
    public $secret = 'tA13pmMdGFy48oyNZHITrAcgZtUFGqCpyrFOL4PgWi0OjfQcqT';
    public $sandbox_host = "https://api-sandbox.dwolla.com/";

    public function createNewAccessToken(){
        $basic_credentials = base64_encode($this->key.':'.$this->secret);
        $ch = curl_init('https://sandbox.dwolla.com/oauth/v2/token');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$basic_credentials, 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec($ch));
        $token = $data->access_token;
        curl_close($ch);
        if($token){
            return Yii::$app->db->createCommand()->update('access_token', ['token'=>$token])->execute();
        }
        return false;
    }

    public function transferMoneyToCustomer($amount){

        $token_obj = AccessToken::findOne(1);
        \DwollaSwagger\Configuration::$access_token = $token_obj->token;// is the access_token i save in DB
        $apiClient = new \DwollaSwagger\ApiClient($this->sandbox_host);
        $dest_funding_url = 'https://api-sandbox.dwolla.com/funding-sources/'.Yii::$app->user->identity->fund_id;
        $bank_funding_id = '9b12d541-63dd-4a93-a5d6-fdfcbbfa9984';
        $source_funding_url = 'https://api-sandbox.dwolla.com/funding-sources/'.$bank_funding_id;
        $transfersApi = new \DwollaSwagger\TransfersApi($apiClient);
        $transfer = $transfersApi->create([
            '_links' => [
                'source' => [
                    'href' => $source_funding_url
                ],
                'destination' => [
                    'href' => $dest_funding_url
                ],
            ],
            'amount' => [
                'currency' => 'USD',
                'value' => $amount
            ],
        ]);
        return $transfer;
    }

    public function createCustomer($data, $customersApi){
        $customerUrl = $customersApi->create([
            'firstName' => $data['name'],
            'lastName' => 'Doe',
            'email' => $data['email'],
            'ipAddress' => '10.10.10.10',
            'type' => 'personal',
            'address1' => '221 Corrected Address St.',
            'address2' => 'Fl 8',
            'city' => 'Ridgewood',
            'state' => 'NY',
            'postalCode' => '11385',
            'dateOfBirth' => '1990-07-11',
            'ssn' => '202-99-1516',
        ]);
        return $customerUrl;
    }

    public function getCustomerFundId($apiClient, $customerUrl){
        $fsApi = new \DwollaSwagger\FundingsourcesApi($apiClient);
        $fundingSources = $fsApi->getCustomerFundingSources($customerUrl);
        $customer_fund_id = $fundingSources->_embedded->{'funding-sources'}[0]->id;
        return $customer_fund_id;
    }
}