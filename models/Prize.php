<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/12/2018
 * Time: 16:30
 */

namespace app\models;
use Yii;
use yii\base\Model;


class Prize extends Model
{

    const ITEMS = 'items';
    const MONEY = 'money';
    const BONUS = 'bonus_points';

    public static $min = 10;
    private static $max = 10000;

    // as i have only 5000 usd in balance i use it sparingly
    public static $min_money_prize = '1';
    public static $max_money_prize = '3';

    public $coefficient = 100;

    public $prizes = [
        'money' => [],
        'bonus_points' => [],
        'items' => ['Car', 'House', 'E-book', 'Guitar'],
    ];

    public function getRandomPrize(){
        $prize_category = array_rand($this->prizes,1);
        if($prize_category){
            if($prize_category == self::ITEMS)
            {
                $key = array_rand($this->prizes[$prize_category],1);
                $prize_item = $this->prizes[$prize_category][$key];
                if(self::sendItemToEmail($prize_item)){
                    Yii::$app->session->setFlash('prize_item_success','Congratulations.You have won <strong>'. $prize_item.'</strong>.Checkout your email');
                }
            }

            else if($prize_category == self::BONUS)
            {
                $random_sum = self::getRandomBonusSum();
                if(User::updateUserBonusPoints($random_sum)){
                    Yii::$app->session->setFlash('prize_bonus_success','Congratulations.You have won <strong>'. $random_sum.'</strong> bonus points');
                }
            }

            else if($prize_category == self::MONEY)
            {
                // gets random money prize sum
                $amount = self::getRandomMoneySum();
                if($amount) {
                    $token_model = new AccessToken();
                    try {
                        if ($token_model->transferMoneyToCustomer($amount)) {
                            Yii::$app->session->setFlash('prize_money_success', 'Congratulations.You have won <strong>' . $amount . ' USD</strong>.Money was transfered to your bank account');
                        }
                    } catch (\Exception $e) {
                        if ($e->getCode() == 401) {
                            $token_model->createNewAccessToken();
                            if ($token_model->transferMoneyToCustomer($amount)) {
                                Yii::$app->session->setFlash('prize_money_success', 'Congratulations.You have won <strong>' . $amount . ' USD</strong>.Money was transfered to your bank account');
                            }
                        }
                    }
                    // convert money prize to bonus points
                    $bonus_points = $this->convertMoneyToPoints($amount);
                    if($bonus_points){
                        if(User::updateUserBonusPoints($bonus_points)) {
                            Yii::$app->session->setFlash('prize_bonus_success', 'Congratulations.You have won <strong>' . $bonus_points . ' </strong> bonus points');
                        }
                    }
                }
            }
        }
    }

    public static function sendItemToEmail($prize_item){
        Yii::$app->mailer->compose('prize', ['prize_item' => $prize_item])
            ->setFrom(['seymour.aliyev@yandex.ru' => 'Prizer'])
            ->setTo(Yii::$app->user->identity->email)
            ->setSubject('Order Response')
            ->send();
        return true;
    }

    public static function getRandomBonusSum(){
        return mt_rand(self::$min, self::$max);
    }

    public static function getRandomMoneySum(){
        $amount = mt_rand(self::$min_money_prize, self::$max_money_prize);
        return number_format($amount,2);
    }

    public function convertMoneyToPoints($amount){
        $bonus = $amount * $this->coefficient;
        return $bonus;
    }
}